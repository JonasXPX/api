package br.uniamerica.prova.contact.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonView;

import br.uniamerica.prova.config.View;
import br.uniamerica.prova.user.domain.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Contact {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonView(View.List.class)
	private Long id;

	@NotBlank
	@JsonView(View.List.class)
	private String name;

	@NotBlank
	@JsonView(View.List.class)
	private String email;

	@NotBlank
	@JsonView(View.List.class)
	private String celphone;

	@ManyToOne
	@JoinColumn(name = "id_user")
	@JsonView(View.List.class)
	private User user;

}
