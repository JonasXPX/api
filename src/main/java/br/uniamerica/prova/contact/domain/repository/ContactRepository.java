package br.uniamerica.prova.contact.domain.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.uniamerica.prova.contact.domain.Contact;
import br.uniamerica.prova.contact.domain.view.ContactView;

public interface ContactRepository extends JpaRepository<Contact, Long> {
	
	
	@Query("select c from Contact c where c.user.id = ?1")
	Page<ContactView> findAllContacts(Long idUser, Pageable pageable);
	
}
