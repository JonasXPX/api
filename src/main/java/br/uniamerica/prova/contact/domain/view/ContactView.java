package br.uniamerica.prova.contact.domain.view;

public interface ContactView {

	Long getId();
	
	String getName();
	
	String getCelphone();
	
	String getEmail();
}
