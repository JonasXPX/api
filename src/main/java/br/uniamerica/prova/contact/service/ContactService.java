package br.uniamerica.prova.contact.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import br.uniamerica.prova.contact.domain.Contact;
import br.uniamerica.prova.contact.domain.repository.ContactRepository;
import br.uniamerica.prova.contact.domain.view.ContactView;
import br.uniamerica.prova.user.domain.User;
import br.uniamerica.prova.user.service.UserService;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ContactService {
	
	private final ContactRepository repository;
	private final UserService userService;
	
	public Contact saveContact(Contact contact) {
		contact.setUser(User
				.builder()
				.id(getUser().getId())
				.build());
		return repository.save(contact);
	}
	
	public Page<ContactView> findContacts(Pageable pageable) {
		return repository.findAllContacts(getUser().getId(), pageable);
	}
	
	public void deleteContact(Long id) {
		repository.deleteById(id);
	}
	
	private String getUsernameFromSecurityContext() {
		return Optional.ofNullable(SecurityContextHolder.getContext())
				.map(SecurityContext::getAuthentication)
				.map(Authentication::getName)
				.orElseThrow();
	}
	
	private User getUser() {
		return userService.findUserByEmail(getUsernameFromSecurityContext());
	}
}
