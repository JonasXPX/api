package br.uniamerica.prova.contact.web;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import br.uniamerica.prova.config.View;
import br.uniamerica.prova.contact.domain.Contact;
import br.uniamerica.prova.contact.domain.view.ContactView;
import br.uniamerica.prova.contact.service.ContactService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("contact")
public class ContactController {

	private final ContactService service;

	@PutMapping({ "", "/" })
	@ResponseBody
	@JsonView(View.List.class)
	public ResponseEntity<Contact> createContact(@RequestBody Contact contact) {
		return ResponseEntity.ok(service.saveContact(contact));
	}

	@PostMapping({ "", "/" })
	@ResponseBody
	@JsonView(View.List.class)
	public ResponseEntity<Contact> saveContact(@RequestBody Contact contact) {
		return ResponseEntity.ok(service.saveContact(contact));
	}

	@GetMapping({ "", "/" })
	@ResponseBody
	public Page<ContactView> findContacts(Pageable pageable) {
		return service.findContacts(pageable);
	}
	
	@DeleteMapping("{id}")
	public void deleteContact(@PathVariable Long id) {
		service.deleteContact(id);
	}

}
