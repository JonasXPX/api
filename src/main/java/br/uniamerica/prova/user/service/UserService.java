package br.uniamerica.prova.user.service;

import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.uniamerica.prova.user.domain.User;
import br.uniamerica.prova.user.domain.repository.UserRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class UserService implements UserDetailsService{

	private final UserRepository repository;
	private final PasswordEncoder encoder;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return Optional
				.ofNullable(findUserByEmail(username))
				.orElseThrow(() -> new UsernameNotFoundException("user " + username + " not found"));
	}
	
	public void createUser(User user) {
		user.setPassword(encoder.encode(user.getPassword()));
		repository.save(user);
	}
	
	public User findUserByEmail(String email) {
		return repository.findOneByEmail(email);
	}
}
