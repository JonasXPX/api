package br.uniamerica.prova.user.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.uniamerica.prova.user.domain.User;
import br.uniamerica.prova.user.service.UserService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("user")
public class UserController {

	private final UserService service;
	
	@PutMapping({"", "/"})
	@ResponseStatus(code = HttpStatus.CREATED)
	public void create(@RequestBody User user) {
		service.createUser(user);
	}
	
	
}
