CREATE table "users" (
	id BIGSERIAL,
	email VARCHAR(50) not null,
	password VARCHAR(255) not null,
	constraint pk_users_id primary key (id),
	constraint uniq_user_email UNIQUE(email)
);

create table contact (
	id BIGSERIAL,
	name VARCHAR(45) not null,
	email VARCHAR(45) not null,
	celphone VARCHAR(45) not null,
	id_user int8 not null,
	constraint pk_contact_id primary key (id),
	constraint fk_contact_id_user foreign key(id_user) references "users"(id)
);
